Informations
===================
----------

Ceci est une application de test "Hello-World"


Environnement
-------------
L'application est basé sur Ionic 2 / cordova


> **Requis:**

> - NodeJs
> - Cordova / Ionic 2

Installation de cordova et Ionic

```
$ npm install -g cordova ionic
$ cordova plateform add browser
```

Build
-------------------
```
$ ionic build browser
```
Un dossier www sera créer à la racine du projet.
C'est sur celui que le serveur doit rooté.
